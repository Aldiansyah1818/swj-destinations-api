const express = require('express');
const router = express.Router();

const DestinationController = require('../controllers/DestinationController');

router.get('/destinations', DestinationController.getDestination);

router.post('/destinations', DestinationController.createDestination);

router.put('/destinations/:destinationId', DestinationController.updateDestination);


module.exports = router;
