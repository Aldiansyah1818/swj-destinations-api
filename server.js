const express = require('express');
const app = express();
const db = require('./config/database');

app.use(express.json());

//Route
app.use('/api', require('./routes/Destination'));

db.sync({ force: false });
app.listen(5001);
