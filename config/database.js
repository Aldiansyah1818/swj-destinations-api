const Sequelize = require('sequelize');

const connectDB = new Sequelize('destinations2', 'postgres', 'secret', {
  dialect: 'postgres',
  host: 'localhost',
  port: 5432,
});

module.exports = connectDB;
