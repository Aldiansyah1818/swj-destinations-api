const { Op } = require('sequelize');
const Destination = require('../models/Destination');


exports.getDestination = (req, res) => {
  Destination.findAll({
    where: {
      deletedAt: {
        [Op.is]: null,
      },
    },
    order: [['createdAt', 'ASC']],
  })
    .then((Destination) => {
      res.send({
        success: true,
        statusCode: 200,
        message: 'Data Destination',
        data: Destination,
      });
    })
    .catch((err) => {
      res.send({
        success: true,
        statusCode: 500,
        message: err.message,
        data: {},
      });
    });
};



exports.createDestination = (req, res) => {
  Destination.create(req.body, {
    fields: ['name', 'description', 'cityId', 'referenceId', 'address', 'openDay', 'opentime', 'image', 'rating', 'latitude', 'longtitude'],
  })
    .then((destination) => {
      res.send({
        success: true,
        statusCode: 200,
        message: 'Created',
        data: destination,
      });
    })
    .catch((err) => {
      res.send({
        success: true,
        statusCode: 400,
        message: err.message,
        data: {},
      });
    });
};

exports.updateDestination = (req, res) => {
  const { destinationId } = req.params;
  const body = req.body;

  Destination.update( {
    name: body.name,
    description: body.description,
    cityId: body.cityId,
    referenceId: body.referenceId,
    address: body.address,
    openDay: body.openDay,
    opentime: body.opentime,
    image: body.image,
    rating: body.rating,
    latitude: body.latitude,
    longtitude: body.longtitude,
    
    },{
      where:{
        id: destinationId,
      },
    }
  )
  .then((destination) => {
    res.send({
      success: true,
      statusCode: 200,
      message: 'Created',
      data: destination,
    });
  })
  .catch((err) => {
    res.send({
      success: true,
      statusCode: 400,
      message: err.message,
      data: {},
    });
  });
};
