const { DataTypes } = require('sequelize');
const Database = require('../config/database');

const DestinationSchema = Database.define('destination', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  name: DataTypes.STRING,
  description: DataTypes.STRING,
  cityId: DataTypes.INTEGER,
  referenceId: DataTypes.INTEGER,
  address: DataTypes.STRING,
  openDay: DataTypes.JSON,
  opentime: DataTypes.DOUBLE,
  image: DataTypes.TEXT,
  rating: DataTypes.FLOAT,
  latitude: DataTypes.STRING,
  longtitude: DataTypes.STRING,
  deletedAt: DataTypes.DATE,
});

module.exports = DestinationSchema;
  